import passport       from 'passport';
import GitHubStrategy from 'passport-github2';

const githubConfig = {
    clientID: '5a59cb95c585e6a40c1a',
    clientSecret: 'e57bb3a55c6c2bc4cff4fdcf46d29ed95443046a',
    callbackURL: "http://localhost:5000/auth/github/callback"
}

passport.use(new GitHubStrategy(githubConfig,
    function(accessToken, refreshToken, profile, done) {
        console.log(profile);
        return done(null, profile);
    }
));

export default passport
