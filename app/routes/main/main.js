// @flow
import passport from '../../services/passport';

class MainScope {
    data : Object;
    vue  : Object;
    constructor() {
        this.data = {
        };

        this.vue = {
            meta: {
                title: 'Simple CI',
            }
        }
    }
}

export default (router: Object) => {
    router.get('/', (req, res, next) => {
        let scope = new MainScope();
        res.render('main/main', scope);
    })

    router.get('/auth/github',
      passport.authenticate('github', { scope: [ 'user:email' ] }));

    router.get('/auth/github/callback',
      passport.authenticate('github', { failureRedirect: '/login' }),
      function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/');
      });
};
